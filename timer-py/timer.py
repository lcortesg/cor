#!usr/bin/env python
# -*- coding: latin-1 -*-

# pip install gspread oauth2client

import pygame
import time
from pygame.locals import*
import gspread
from oauth2client.service_account import ServiceAccountCredentials

# ID parameters

ID_MAX = 5
ID_MIN = 0
ID_DEFAULT = 2
ID = ID_DEFAULT


# Online check

try :
    scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name('tokens/client_secret.json', scope)
    #creds = ServiceAccountCredentials.from_json_keyfile_name('tokens/raspi1.json', scope)
    #creds = ServiceAccountCredentials.from_json_keyfile_name('tokens/raspi2.json', scope)
    #creds = ServiceAccountCredentials.from_json_keyfile_name('tokens/raspi3.json', scope) 
    #creds = ServiceAccountCredentials.from_json_keyfile_name('tokens/pc1.json', scope)
    #creds = ServiceAccountCredentials.from_json_keyfile_name('tokens/pc2.json', scope)
    #creds = ServiceAccountCredentials.from_json_keyfile_name('tokens/pc3.json', scope)
    client = gspread.authorize(creds)
    online = True
    llamar = True
except :
    online = False
    llamar = False

# Colour definitions

NEGRO = (0, 0, 0)
BLANCO = (255, 255, 255)
VERDE = (0, 255, 0)
ROJO = (255, 0, 0)
color=(0,0,0)

# Spreadsheet definitions

HOJA_RESUMEN = 0
HOJA_LLAMADOS = 8
HOJA_MENSAJES = 9
HOJA_INFO = 10


# State machine definition

ESTADO=['Football_RC','Junior','Robotracer','Sumo_Auto','Sumo_RC','Micromouse','Open']
mensaje=''
answer = {}
lista = {}
lista_mensajes = {}
continuar = True
timer = True
puntajes = False
informaciones = False
timing = True
timing_mensajes = True
actualizar = True
actualizar_info = False
mostrar_mensajes = True

# Time definitions

tiempo = 15
T1=time.time()
TM1=time.time()
TI1=time.time()

pygame.init()
pygame.mouse.set_visible(0)
pantalla = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)


# Font parameters

pygame.font.init()


FONT_ALT = True;

if (FONT_ALT) :
    #fuente = 'fonts/AvenirNextLTPro-Regular.otf'
    #fuente = 'fonts/Coves-Light.otf'
    #fuente = 'fonts/Calibri-Light.ttf'
    #fuente = 'fonts/Raleway-Light.ttf'
    #fuente = 'fonts/Raleway-Regular.ttf'
    #fuente = 'fonts/Roboto-Condensed.ttf'
    #fuente = 'fonts/Roboto-Light.ttf'
    #fuente = 'fonts/Roboto-Medium.ttf'
    #fuente = 'fonts/Roboto-Regular.ttf'
    #fuente = 'fonts/Roboto-Thin.ttf'
    #fuente = 'fonts/Ubuntu-Light.ttf'
    #fuente = 'fonts/Inconsolata-Regular.ttf'
    #fuente = 'fonts/FiraMono-Regular.ttf'
    #fuente = 'fonts/FiraCode-Regular.ttf'
    #fuente = 'fonts/RobotoMono-Regular.ttf'
    #fuente = 'fonts/SourceCodeVariable-Roman.ttf'
    fuente = 'fonts/Ubuntu-Mono.ttf'
    #fuente = 'fonts/Vera-Regular.ttf'
    font20 = pygame.font.Font(fuente, 20)
    font25 = pygame.font.Font(fuente, 25)
    font30 = pygame.font.Font(fuente, 30)
    font35 = pygame.font.Font(fuente, 35)
    font40 = pygame.font.Font(fuente, 40)
    font45 = pygame.font.Font(fuente, 45)
    font50 = pygame.font.Font(fuente, 50)
    font55 = pygame.font.Font(fuente, 55)
    font60 = pygame.font.Font(fuente, 60)
    font65 = pygame.font.Font(fuente, 65)
    font70 = pygame.font.Font(fuente, 70)
    font75 = pygame.font.Font(fuente, 75)
    font80 = pygame.font.Font(fuente, 80)
    font90 = pygame.font.Font(fuente, 90)
    font100 = pygame.font.Font(fuente, 100)
    font110 = pygame.font.Font(fuente, 110)
    font120 = pygame.font.Font(fuente, 120)
    font150 = pygame.font.Font(fuente, 150)

if (not FONT_ALT) :
    font40 = pygame.font.Font(None, 40)
    font50 = pygame.font.Font(None, 50)
    font60 = pygame.font.Font(None, 60)
    font70 = pygame.font.Font(None, 70)
    font80 = pygame.font.Font(None, 80)
    font100 = pygame.font.Font(None, 100)
    font120 = pygame.font.Font(None, 120)
    font150 = pygame.font.Font(None, 150)



# Timer parameters

POS_X_TIMER = 200
POS_Y_TIMER = 320

POS_X_TIMER_TT = 50
POS_Y_TIMER_TT = 50

POS_X_TIMER_ST = 950
POS_Y_TIMER_ST = 50

POS_X_TIMER_UP = 250
POS_X_TIMER_UP_S = 250
POS_Y_TIMER_UP = 210

POS_X_TIMER_DOWN = 250
POS_X_TIMER_DOWN_S = 250
POS_Y_TIMER_DOWN = 500

TIMER_FONT_SIZE = font110
TIMER_TITLE_SIZE = font70
TIMER_NEXT_SIZE = font40

# Scores parameters

POS_X_SCORE_TT = 50
POS_Y_SCORE_TT = 50

POS_X_SCORE_ST = 950
POS_Y_SCORE_ST = 50

POS_X_TIME_ACT = 1320
POS_Y_TIME_ACT = 730

POS_X_TIME_STOP = 1280
POS_Y_TIME_STOP = 730

SCORE_TITLE_SIZE = font70
SCORE_FONT_SIZE = font40
SCORE_MESSAGE_SIZE = font30
SCORE_TIME_SIZE = font30


# Informations parameters

INFO_TITLE_SIZE = font60
INFO_FONT_SIZE = font40


while continuar:

    while informaciones :

        Background = pygame.image.load('img/info.png').convert()
        Title = INFO_TITLE_SIZE.render('Informaciones importantes - CoR 2019', True, BLANCO)
        
        miauf = True
        pantalla.fill(BLANCO)
        pantalla.blit(Background,(0, 0))
        pantalla.blit(Title,(100,50))
        #pantalla.blit(SubT,(800,50))
        
        if actualizar_info :
            try :
                if int(client.open("puntajes").get_worksheet(HOJA_INFO).cell(1, 2).value):
                    infos = client.open("puntajes").get_worksheet(HOJA_INFO).col_values(3)
                    actualizar_info = False
                    online = True
                else :
                    informaciones = False
                    puntajes = True
                    actualizar_info = False
                    T1=time.time()
                    TM1=time.time()
                    TI1=time.time()
                    online = True
            except :
                online = False
        
        cont = 0
        if online :
            for i in infos :
                if len(infos) == 1:
                    LI = INFO_FONT_SIZE.render('- ' + i.encode('latin-1').strip(), True, BLANCO)
                    pantalla.blit(LI,(50, 310))

                if len(infos) == 2:
                    LI = INFO_FONT_SIZE.render('- ' + i.encode('latin-1').strip(), True, BLANCO)
                    pantalla.blit(LI,(50, 270+cont*80))

                if len(infos) == 3:
                    LI = INFO_FONT_SIZE.render('- ' + i.encode('latin-1').strip(), True, BLANCO)
                    pantalla.blit(LI,(50, 230+cont*80))

                if len(infos) == 4:
                    LI = INFO_FONT_SIZE.render('- ' + i.encode('latin-1').strip(), True, BLANCO)
                    pantalla.blit(LI,(50, 190+cont*80))

                if len(infos) == 5:
                    LI = INFO_FONT_SIZE.render('- ' + i.encode('latin-1').strip(), True, BLANCO)
                    pantalla.blit(LI,(50, 150+cont*80))
                cont += 1
        elif not online :
            ERROR = font120.render('ERROR, NO HAY CONEXION A INTERNET!', True, BLANCO)
            pantalla.blit(ERROR,(50, 310))

        pygame.display.update()

        while miauf:
            TI2 = time.time()
            if round((TI2-TI1)%60) >= 60 :
                miauf = False
                actualizar_info = True

            for event in pygame.event.get():
                if event.type == KEYDOWN:

                    if event.key == K_t:
                        del lista[:]
                        answer.clear()
                        actualizar = True
                        timer = True
                        puntajes = False
                        informaciones = False
                        miauf = False
                        
                    elif event.key == K_ESCAPE:
                        del lista[:]
                        answer.clear()
                        miauf = False
                        continuar = False
                        timer = False
                        puntajes = False
                        informaciones = False

                elif event.type == QUIT:
                    miauf = False
                    continuar = False
                    timer = False
                    puntajes = False
                    informaciones = False

    while puntajes :
        Background = pygame.image.load('img/'+ESTADO[ID].lower()+'.png').convert()
        Title = SCORE_TITLE_SIZE.render('Puntajes CoR 2019', True, BLANCO)
        SubT = SCORE_TITLE_SIZE.render(ESTADO[ID].replace('_',' '), True, BLANCO)

        tiempo_imprimir = str(tiempo) if timing else 'STOP'

        ActTime = SCORE_TIME_SIZE.render(tiempo_imprimir, True, BLANCO)

        miaup = True
        pantalla.fill(BLANCO)
        pantalla.blit(Background, (0, 0))
        pantalla.blit(Title,(POS_X_SCORE_TT, POS_Y_SCORE_TT))
        pantalla.blit(SubT,(POS_X_SCORE_ST, POS_Y_SCORE_ST))

        if timing :
            pantalla.blit(ActTime,(POS_X_TIME_ACT, POS_Y_TIME_ACT))
        else : 
            pantalla.blit(ActTime,(POS_X_TIME_STOP, POS_Y_TIME_STOP))


        if actualizar :
            TI2 = time.time()
            if round((TI2-TI1)%60) >= 60 :
                try :
                    if int(client.open("puntajes").get_worksheet(HOJA_INFO).cell(1, 2).value) :
                        informaciones = True
                        actualizar_info = True
                        puntajes = False
                        TI1=time.time()
                        online = True
                except :
                    online = False
            elif online :
                answer.clear()
                decimal = True if (ID == 2 or ID == 5) else False
                signo = 1 if decimal else -1 
                resumen = client.open("puntajes").get_worksheet(HOJA_RESUMEN)
                equipo = resumen.col_values(ID*2+1)
                puntaje = resumen.col_values(ID*2+2)
                for i in range(1, len(equipo)) :
                    answer[equipo[i]] = float(puntaje[i]) if decimal else int(puntaje[i])
                lista = (sorted(answer.items(), key=lambda kv: (signo*kv[1], kv[0])))
                pygame.display.set_caption("Puntajes")
                if mostrar_mensajes :
                    mensajes = client.open("puntajes").get_worksheet(HOJA_MENSAJES)
                    lista_mensajes = mensajes.col_values(2)
                mensaje = SCORE_MESSAGE_SIZE.render('* ' + lista_mensajes[ID].encode('latin-1').strip() + ' *', True, BLANCO)
                pantalla.blit(mensaje,(50,730))
                actualizar = False


        if online :

            cont = 0

            if(len(lista)<=3):
                SCORE_FONT_SIZE = font60
                h = 400
                v = 200
                for i in lista:
                    TT = SCORE_FONT_SIZE.render(str(cont+1)+'. '+i[0].encode('latin-1').strip()+': '+str(i[1]), True, BLANCO)
                    pantalla.blit(TT,(h, v+(100*int(cont))))
                    cont += 1

            if(len(lista)==4):
                SCORE_FONT_SIZE = font60
                h = 400
                v = 180
                for i in lista:
                    TT = SCORE_FONT_SIZE.render(str(cont+1)+'. '+i[0].encode('latin-1').strip()+': '+str(i[1]), True, BLANCO)
                    pantalla.blit(TT,(h, v+(100*int(cont))))
                    cont += 1

            if(len(lista)>4 and len(lista)<=6):
                SCORE_FONT_SIZE = font45
                for i in lista:
                    TT = SCORE_FONT_SIZE.render(str(cont+1)+'. '+i[0].encode('latin-1').strip()+': '+str(i[1]), True, BLANCO)
                    if cont >= 3 :
                        h = 700
                        v = -100
                    else :
                        h = 100
                        v = 200
                    pantalla.blit(TT,(h, v+(100*int(cont))))
                    cont += 1

            if(len(lista)>6 and len(lista)<=8):
                SCORE_FONT_SIZE = font45
                for i in lista:
                    TT = SCORE_FONT_SIZE.render(str(cont+1)+'. '+i[0].encode('latin-1').strip()+': '+str(i[1]), True, BLANCO)
                    if cont >= 4 :
                        h = 700
                        v = -250
                    else :
                        h = 100
                        v = 150
                    pantalla.blit(TT,(h, v+(100*int(cont))))
                    cont += 1

            if(len(lista)>8 and len(lista)<=10):
                SCORE_FONT_SIZE = font45
                for i in lista:
                    TT = SCORE_FONT_SIZE.render(str(cont+1)+'. '+i[0].encode('latin-1').strip()+': '+str(i[1]), True, BLANCO)
                    if cont >= 5 :
                        h = 700
                        v = -250
                    else :
                        h = 100
                        v = 150
                    pantalla.blit(TT,(h, v+(80*int(cont))))
                    cont += 1

            if(len(lista)>10 and len(lista)<=12):
                SCORE_FONT_SIZE = font40
                for i in lista:
                    TT = SCORE_FONT_SIZE.render(str(cont+1)+'. '+i[0].encode('latin-1').strip()+': '+str(i[1]), True, BLANCO)
                    if cont >= 6 :
                        h = 700
                        v = -270
                    else :
                        h = 100
                        v = 150
                    pantalla.blit(TT,(h, v+(70*int(cont))))
                    cont += 1

            if(len(lista)>12 and len(lista)<=14):
                SCORE_FONT_SIZE = font40
                for i in lista:
                    TT = SCORE_FONT_SIZE.render(str(cont+1)+'. '+i[0].encode('latin-1').strip()+': '+str(i[1]), True, BLANCO)
                    if cont >= 7 :
                        h = 700
                        v = -270
                    else :
                        h = 100
                        v = 150
                    pantalla.blit(TT,(h, v+(60*int(cont))))
                    cont += 1

            if(len(lista)>14 and len(lista)<=16):
                SCORE_FONT_SIZE = font35
                for i in lista:
                    TT = SCORE_FONT_SIZE.render(str(cont+1)+'. '+i[0].encode('latin-1').strip()+': '+str(i[1]), True, BLANCO)
                    if cont >= 8 :
                        h = 700
                        v = -250
                    else :
                        h = 100
                        v = 150
                    pantalla.blit(TT,(h, v+(50*int(cont))))
                    cont += 1

            if(len(lista)>16 and len(lista)<=18):
                SCORE_FONT_SIZE = font35
                for i in lista:
                    TT = SCORE_FONT_SIZE.render(str(cont+1)+'. '+i[0].encode('latin-1').strip()+': '+str(i[1]), True, BLANCO)
                    if (cont<18 and cont >=9):
                        h = 700
                        v = -300
                    else:
                        h = 100
                        v = 150
                    pantalla.blit(TT,(h, v+(50*int(cont))))
                    cont += 1

            if (len(lista)>18 and len(lista)<=21):
                SCORE_FONT_SIZE = font25
                for i in lista:
                    TT = SCORE_FONT_SIZE.render(str(cont+1)+'. '+i[0].encode('latin-1').strip()+': '+str(i[1]), True, BLANCO)
                    if cont >= 14:
                        h = 900
                        v = -690
                    elif (cont<16 and cont >=7):
                        h = 470
                        v = -270
                    else:
                        h = 50
                        v = 150
                    pantalla.blit(TT,(h, v+(60*int(cont))))
                    cont += 1

            if (len(lista)>21 and len(lista)<=24):
                SCORE_FONT_SIZE = font25
                for i in lista:
                    TT = SCORE_FONT_SIZE.render(str(cont+1)+'. '+i[0].encode('latin-1').strip()+': '+str(i[1]), True, BLANCO)
                    if cont >= 16:
                        h = 900
                        v = -650
                    elif (cont<16 and cont >=8):
                        h = 470
                        v = -250
                    else:
                        h = 50
                        v = 150
                    pantalla.blit(TT,(h, v+(50*int(cont))))
                    cont += 1

            if (len(lista)>24 and len(lista)<=27):
                SCORE_FONT_SIZE = font25
                for i in lista:
                    TT = SCORE_FONT_SIZE.render(str(cont+1)+'. '+i[0].encode('latin-1').strip()+': '+str(i[1]), True, BLANCO)
                    if cont >= 18:
                        h = 900
                        v = -750
                    elif (cont<18 and cont >=9):
                        h = 470
                        v = -300
                    else:
                        h = 50
                        v = 150
                    pantalla.blit(TT,(h, v+(50*int(cont))))
                    cont += 1

        elif not online :
            ERROR = font120.render('ERROR, NO HAY CONEXION A INTERNET!', True, BLANCO)
            pantalla.blit(ERROR,(50, 310))

        pygame.display.update()


        while miaup:
            if timing_mensajes :
                TM2 = time.time()
                if round((TM2-TM1)%60) >= 60 :
                    TM1=time.time()
                    miaup = False
                    actualizar = True
                    mostrar_mensajes = True
                else :
                    mostrar_mensajes = False

            if timing :
                T2 = time.time()
                if round((T2-T1)%60) >= tiempo :
                    T1=time.time()
                    miaup = False
                    actualizar = True
                    mostrar_mensajes = False
                    if ID < ID_MAX :
                        ID += 1 
                    else :
                        ID = 0
            
            for event in pygame.event.get():
                if event.type == KEYDOWN:

                    if event.key == K_RETURN:
                        T1=time.time()
                        del lista[:]
                        actualizar = True
                        miaup = False
                        timing = True
                            
                    elif event.key == K_SPACE:
                        miaup = False
                        timing = not timing
                        T1 = time.time()

                    elif event.key == K_UP:
                        timing = True
                        T1=time.time()
                        miaup = False
                        if tiempo < 60 :
                            tiempo += 5
                        else :
                            tiempo = 60

                    elif event.key == K_DOWN:
                        timing = True
                        T1=time.time()
                        miaup = False
                        if tiempo > 5 :
                            tiempo -= 5
                        else :
                            tiempo = 5

                    elif event.key == K_RIGHT:
                        T1=time.time()
                        actualizar = True
                        answer.clear()
                        miaup = False
                        del lista[:]
                        if ID < ID_MAX :
                            ID += 1            
                        else :
                            ID = 0

                    elif event.key == K_LEFT:
                        T1=time.time()
                        actualizar = True
                        answer.clear()
                        del lista[:]
                        miaup = False
                        if ID > 1 :
                            ID -= 1
                        else :
                            ID = ID_MAX

                    elif event.key == K_f:
                        T1=time.time()
                        del lista[:]
                        actualizar = True
                        ID = 0
                        miaup = False

                    elif event.key == K_j:
                        T1=time.time()
                        del lista[:]
                        actualizar = True
                        ID = 1
                        miaup = False

                    elif event.key == K_r:
                        T1=time.time()
                        del lista[:]
                        actualizar = True
                        ID = 2
                        miaup = False

                    elif event.key == K_s:
                        T1=time.time()
                        del lista[:]
                        actualizar = True
                        ID = 3
                        miaup = False

                    elif event.key == K_z:
                        T1=time.time()
                        del lista[:]
                        actualizar = True
                        ID = 4
                        miaup = False

                    elif event.key == K_m:
                        T1=time.time()
                        del lista[:]
                        actualizar = True
                        ID = 5
                        miaup = False

                    #elif event.key == K_o:
                    #    T1=time.time()
                    #    del lista[:]
                    #    actualizar = True
                    #    ID = 6
                    #    miaup = False

                    elif event.key == K_t:
                        del lista[:]
                        answer.clear()
                        actualizar = True
                        timer = True
                        puntajes = False
                        miaup = False
                        
                    elif event.key == K_ESCAPE:
                        del lista[:]
                        answer.clear()
                        miaup = False
                        continuar = False
                        timer = False
                        puntajes = False

                elif event.type == QUIT:
                    miaup = False
                    continuar = False
                    timer = False
                    puntajes = False
        
    while timer :
        
        POS_X_TIMER_UP = POS_X_TIMER_UP_S if (ID == 3 or ID == 4) else 250
        POS_X_TIMER_DOWN = POS_X_TIMER_DOWN_S if (ID == 3 or ID == 4) else 250

        Background = pygame.image.load('img/'+ESTADO[ID].lower()+'.png').convert()
        pygame.display.set_caption("Cronometro")
        Title = TIMER_TITLE_SIZE.render("Timer CoR 2019", True, BLANCO)
        SubT = TIMER_TITLE_SIZE.render(ESTADO[ID].replace('_',' '), True, BLANCO)
        TT = TIMER_FONT_SIZE.render("Tiempo: 00:00.000", True, BLANCO)
        miaut = True
        hecho = False
        pantalla.fill(BLANCO)
        pantalla.blit(Background, (0, 0))
        pantalla.blit(Title,(POS_X_TIMER_TT,POS_Y_TIMER_TT))
        pantalla.blit(SubT,(POS_X_TIMER_ST,POS_Y_TIMER_ST))
        pantalla.blit(TT,(POS_X_TIMER, POS_Y_TIMER))
        pygame.display.update()

        


        if (llamar and online) :
            try :
                llamado = client.open("puntajes").get_worksheet(HOJA_LLAMADOS)
                llamado_u = TIMER_NEXT_SIZE.render('Ahora : '+ llamado.cell(ID+1, 2).value.encode('latin-1').strip(), True, BLANCO)
                llamado_d = TIMER_NEXT_SIZE.render('Sigue : '+ llamado.cell(ID+1, 3).value.encode('latin-1').strip(), True, BLANCO)
                llamar = False
                online = True
            except :
                llamar = False
                online = False

        if online :
            pantalla.blit(llamado_u,(POS_X_TIMER_UP, POS_Y_TIMER_UP))
            pantalla.blit(llamado_d,(POS_X_TIMER_DOWN, POS_Y_TIMER_DOWN))
            pygame.display.update()

        while miaut:
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_SPACE:
                        miaut = False

                    elif event.key == K_RETURN:
                        miaut = False
                        hecho = True
                        if online :
                            llamar = True

                    elif event.key == K_p:
                        if online :
                            puntajes = True
                            timer = False 
                            hecho = True
                            miaut = False
                            llamar = True

                    elif event.key == K_f:
                        ID = 0
                        miaut = False
                        hecho = True
                        llamar = True

                    elif event.key == K_j:
                        ID = 1
                        miaut = False
                        hecho = True
                        llamar = True

                    elif event.key == K_r:
                        ID = 2
                        miaut = False
                        hecho = True
                        llamar = True

                    elif event.key == K_s:
                        ID = 3
                        miaut = False
                        hecho = True
                        llamar = True

                    elif event.key == K_z:
                        ID = 4
                        miaut = False
                        hecho = True
                        llamar = True

                    elif event.key == K_m:
                        ID = 5
                        miaut = False
                        hecho = True
                        llamar = True

                    #elif event.key == K_o:
                    #    ID = 6
                    #    miaut = False
                    #    hecho = True
                    #    llamar = True

                    elif event.key == K_RIGHT:
                        llamar = True
                        if ID < ID_MAX :
                            ID += 1
                            miaut = False
                            hecho = True
                        else :
                            ID = 0
                            miaut = False
                            hecho = True

                    elif event.key == K_LEFT:
                        llamar = True
                        if ID > 1 :
                            ID -= 1
                            miaut = False
                            hecho = True

                        else :
                            ID = ID_MAX
                            miaut = False
                            hecho = True
                
                    elif event.key == K_ESCAPE:
                        continuar = False
                        hecho = True
                        miaut = False
                        timer = False
                        puntajes = False
     
                elif event.type == QUIT:
                    hecho = True
                    miaut = False
                    continuar = False
                    timer = False
                    puntajes = True
        T1 = time.time()
        Control1 = False
        Control2 = False
        Control3 = 0
        RText = ''
        while not hecho:
            for evento in pygame.event.get():
                if evento.type == pygame.QUIT:
                    hecho = True
                elif evento.type == KEYDOWN:
                    if evento.key == K_SPACE and not Control1:
                        try:
                            RText = OText
                            Control1 = True
                            #Control2 = False
                            Control3 = [int(Min),float(Sec)]
                        except:
                            pass
                    elif evento.key == K_SPACE and Control1:
                        Control1 = False
                        Control2 = True
                        T1 = time.time()
                    elif evento.key == K_RETURN:
                        hecho = True
                        llamar = True
                    elif evento.key == K_ESCAPE:
                        hecho = True
                        continuar = False

            if not hecho:
                pantalla.fill(BLANCO)
                pantalla.blit(Background, (0, 0))
                pantalla.blit(Title,(POS_X_TIMER_TT, POS_Y_TIMER_TT))
                pantalla.blit(SubT,(POS_X_TIMER_ST, POS_Y_TIMER_ST))
                if (online) :
                    pantalla.blit(llamado_u,(POS_X_TIMER_UP, POS_Y_TIMER_UP))
                    pantalla.blit(llamado_d,(POS_X_TIMER_DOWN, POS_Y_TIMER_DOWN))

                T2 = time.time()
                if not Control2:
                    Min = str(int((T2-T1)/60))
                    Sec = str(round((T2-T1)%60,3))
                else:
                    Min = str(int(Control3[0]+(T2-T1)/60))
                    Sec = str(round(Control3[1]+(T2-T1)%60,3))
                if int(Min) < 10:
                    Min = '0' + Min
                if float(Sec)<10:
                    Sec = '0' + Sec

                OText = "Tiempo: {0}:{1}".format(Min, Sec)

                if not Control1:
                    Text = TIMER_FONT_SIZE.render(OText, True, BLANCO)
                else:
                    Text = TIMER_FONT_SIZE.render(RText, True, BLANCO)
                pantalla.blit(Text, [POS_X_TIMER, POS_Y_TIMER])

                pygame.display.update()

pygame.quit()


