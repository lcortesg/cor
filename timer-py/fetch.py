import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pprint

# pip install gspread oauth2client

cat_max = 5
cat_min = 0
answer = {}
lista = {}

scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('tokens/client_secret.json', scope)
client = gspread.authorize(creds)

while True:
    try:
        cat  = int(input("Escoge el numero de hoja (0 - 5) : "))
        if cat >= cat_min and cat <= cat_max:
            break
        else :
            print("El numero de hoja debe estar entre 0 y 5")
            continue
    except ValueError:
        print("Ingresa solo valores numericos entre 0 y 5")
        continue
    else:
        continue
        
resumen = client.open("puntajes").get_worksheet(0)
equipo = resumen.col_values(cat*2+1)
puntaje = resumen.col_values(cat*2+2)

decimal = True if (cat == 2 or cat == 5) else False
signo = 1 if decimal else -1

for i in range(1, len(equipo)) :
    answer[equipo[i]] = float(puntaje[i]) if decimal else int(puntaje[i])
    #answer[equipo[i]] = format(float(puntaje[i]),'.3f') if decimal else int(puntaje[i])
lista = (sorted(answer.items(), key=lambda kv: (signo*kv[1], kv[0])))

print(lista)
print ("El ganador es: "+lista[0][0])