import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pprint

cat_max = 6
cat_min = 0
answer = {}
lista = {}

scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('tokens/client_secret.json', scope)
client = gspread.authorize(creds)

while True:
    try:
        cat  = int(input("Escoge el numero de hoja (0 - 6) : "))
        if cat >= cat_min and cat <= cat_max:
            break
        else :
            print("El numero de hoja debe estar entre 0 y 6")
            continue
    except ValueError:
        print("Ingresa solo valores numericos")
        continue
    else:
        continue
        
sheet = client.open("puntajes").get_worksheet(cat)
document = sheet.get_all_records()

#pp = pprint.PrettyPrinter()
#pp.pprint(document)

decimal = True if (cat == 2 or cat == 4) else False
signo = 1 if decimal else -1

for line in document:
    answer[str(line['Equipo'])] = format(float(line['Puntaje']),'.3f') if decimal else int(line['Puntaje'])
lista = (sorted(answer.items(), key=lambda kv: (signo*kv[1], kv[0])))
print(lista)