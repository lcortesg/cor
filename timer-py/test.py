#!usr/bin/env python
# -*- coding: cp1252 -*-

# pip install gspread oauth2client

import pygame
import time
from pygame.locals import*
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pprint
from shutil import copyfile

ID = 4

try :
    scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name('tokens/client_secret.json', scope)
    #creds = ServiceAccountCredentials.from_json_keyfile_name('tokens/raspi1.json', scope)
    #creds = ServiceAccountCredentials.from_json_keyfile_name('tokens/raspi2.json', scope)
    #creds = ServiceAccountCredentials.from_json_keyfile_name('tokens/raspi3.json', scope) 
    #creds = ServiceAccountCredentials.from_json_keyfile_name('tokens/pc1.json', scope)
    #creds = ServiceAccountCredentials.from_json_keyfile_name('tokens/pc2.json', scope)
    #creds = ServiceAccountCredentials.from_json_keyfile_name('tokens/pc3.json', scope)
    client = gspread.authorize(creds)
    online = True
    llamar = True
except :
    online = False
    llamar = False

NEGRO = (0, 0, 0)
BLANCO = (255, 255, 255)
VERDE = (0, 255, 0)
ROJO = (255, 0, 0)
color=(0,0,0)

HOJA_RESUMEN = 7
HOJA_LLAMADOS = 8
HOJA_MENSAJES = 9
HOJA_INFO = 10


categorias=['Football','Junior','Micromouse','Open','Robotracer','Sumo_Auto','Sumo_RC']
ESTADO=''
mensaje=''
answer = {}
lista = {}
lista_mensajes = {}
continuar = True
timer = True
puntajes = False
informaciones = False
timing = True
timing_mensajes = True
actualizar = True
actualizar_info = False
mostrar_mensajes = True

tiempo = 15
ID_MAX = 6
ID_MIN = 0
T1=time.time()
TM1=time.time()
TF1=time.time()
TI1=time.time()

pygame.init()
pygame.mouse.set_visible(0)
pantalla = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)

font40 = pygame.font.Font(None, 40)
font50 = pygame.font.Font(None, 50)
font60 = pygame.font.Font(None, 60)
font70 = pygame.font.Font(None, 70)
font80 = pygame.font.Font(None, 80)
font100 = pygame.font.Font(None, 100)
font120 = pygame.font.Font(None, 120)
font150 = pygame.font.Font(None, 150)

while continuar:

    while informaciones :

        Background = pygame.image.load('img/info.png').convert()
        Title = font70.render('Informaciones importantes - CoR 2019', True, BLANCO)
        #SubT = font70.render('Novedades y actualizaciones', True, BLANCO)

        
        miauf = True
        pantalla.fill(BLANCO)
        pantalla.blit(Background,(0, 0))
        pantalla.blit(Title,(100,50))
        #pantalla.blit(SubT,(800,50))
        
        if actualizar_info :
            try :
                if int(client.open("puntajes").get_worksheet(HOJA_INFO).cell(1, 2).value):
                    infos = client.open("puntajes").get_worksheet(HOJA_INFO).col_values(3)
                    actualizar_info = False
                    online = True
                else :
                    informaciones = False
                    puntajes = True
                    actualizar_info = False
                    T1=time.time()
                    TM1=time.time()
                    TI1=time.time()
                    online = True
            except :
                online = False
        
        cont = 0
        if online :
            for i in infos :
                if len(infos) == 1:
                    LI = font60.render('- ' + str(i), True, BLANCO)
                    pantalla.blit(LI,(50, 310))

                if len(infos) == 2:
                    LI = font60.render('- ' + str(i), True, BLANCO)
                    pantalla.blit(LI,(50, 270+cont*80))

                if len(infos) == 3:
                    LI = font60.render('- ' + str(i), True, BLANCO)
                    pantalla.blit(LI,(50, 230+cont*80))

                if len(infos) == 4:
                    LI = font60.render('- ' + str(i), True, BLANCO)
                    pantalla.blit(LI,(50, 190+cont*80))

                if len(infos) == 5:
                    LI = font60.render('- ' + str(i), True, BLANCO)
                    pantalla.blit(LI,(50, 150+cont*80))
                cont += 1
        elif not online :
            ERROR = font120.render('ERROR, NO HAY CONEXION A INTERNET!', True, BLANCO)
            pantalla.blit(ERROR,(50, 310))

        pygame.display.update()

        while miauf:
            TF2 = time.time()
            if round((TF2-TF1)%60) >= 60 :
                miauf = False
                actualizar_info = True

            for event in pygame.event.get():
                if event.type == KEYDOWN:

                    if event.key == K_t:
                        del lista[:]
                        answer.clear()
                        actualizar = True
                        timer = True
                        puntajes = False
                        informaciones = False
                        miauf = False
                        
                    elif event.key == K_ESCAPE:
                        del lista[:]
                        answer.clear()
                        miauf = False
                        continuar = False
                        timer = False
                        puntajes = False
                        informaciones = False

                elif event.type == QUIT:
                    miauf = False
                    continuar = False
                    timer = False
                    puntajes = False
                    informaciones = False

    while puntajes :
        ESTADO = categorias[ID]
        Background = pygame.image.load('img/'+ESTADO.lower()+'.png').convert()
        Title = font70.render('CoR 2019', True, BLANCO)
        SubT = font70.render(ESTADO.replace('_',' '), True, BLANCO)

        tiempo_imprimir = str(tiempo) if timing else 'STOP'

        ActTime = font40.render(tiempo_imprimir, True, BLANCO)

        miaup = True
        pantalla.fill(BLANCO)
        pantalla.blit(Background, (0, 0))
        pantalla.blit(Title,(100,50))
        pantalla.blit(SubT,(1000,50))

        if timing :
            pantalla.blit(ActTime,(1320,730))
        else : 
            pantalla.blit(ActTime,(1280,730))


        if actualizar :
            TI2 = time.time()
            if round((TI2-TI1)%60) >= 60 :
                try :
                    if int(client.open("puntajes").get_worksheet(HOJA_INFO).cell(1, 2).value) :
                        informaciones = True
                        actualizar_info = True
                        puntajes = False
                        TF1=time.time()
                        online = True
                except :
                    online = False
            elif online :
                answer.clear()
                decimal = True if (ID == 2 or ID == 4) else False
                signo = 1 if decimal else -1 
                resumen = client.open("puntajes").get_worksheet(HOJA_RESUMEN)
                equipo = resumen.col_values(ID*2+1)
                puntaje = resumen.col_values(ID*2+2)
                for i in range(1, len(equipo)) :
                    #answer[str(equipo[i])] = format(float(puntaje[i]),'.3f') if decimal else int(puntaje[i])
                    answer[str(equipo[i])] = float(puntaje[i]) if decimal else int(puntaje[i])
                lista = (sorted(answer.items(), key=lambda kv: (signo*kv[1], kv[0])))
                pygame.display.set_caption("Puntajes")
                if mostrar_mensajes :
                    mensajes = client.open("puntajes").get_worksheet(HOJA_MENSAJES)
                    lista_mensajes = mensajes.col_values(2)
                mensaje = font40.render('* ' + str(lista_mensajes[ID]) + ' *', True, BLANCO)
                pantalla.blit(mensaje,(50,730))
                actualizar = False


        if online :
            cont=0
            h=0
            v=0
            for i in lista:
                TT = font50.render(str(i).strip("(").strip(")").replace(","," :"), True, BLANCO)
                if cont >= 16:
                    h = 950
                    v = -650
                elif (cont<16 and cont >=8):
                    h = 550
                    v = -250
                else:
                    h = 100
                    v = 150
                pantalla.blit(TT,(h, v+(50*int(cont))))
                cont += 1

        elif not online :
            ERROR = font120.render('ERROR, NO HAY CONEXION A INTERNET!', True, BLANCO)
            pantalla.blit(ERROR,(50, 310))

        pygame.display.update()


        while miaup:
            if timing_mensajes :
                TM2 = time.time()
                if round((TM2-TM1)%60) >= 60 :
                    miaup = False
                    actualizar = True
                    mostrar_mensajes = True
                else :
                    mostrar_mensajes = False

            if timing :
                T2 = time.time()
                if round((T2-T1)%60) >= tiempo :
                    T1=time.time()
                    miaup = False
                    actualizar = True
                    mostrar_mensajes = False
                    if ID < 6 :
                        ID += 1 
                    else :
                        ID = 0
            
            for event in pygame.event.get():
                if event.type == KEYDOWN:

                    if event.key == K_RETURN:
                        T1=time.time()
                        del lista[:]
                        actualizar = True
                        miaup = False
                        timing = True
                            
                    elif event.key == K_SPACE:
                        miaup = False
                        timing = not timing
                        T1 = time.time()

                    elif event.key == K_UP:
                        timing = True
                        T1=time.time()
                        miaup = False
                        if tiempo < 60 :
                            tiempo += 5
                        else :
                            tiempo = 60

                    elif event.key == K_DOWN:
                        timing = True
                        T1=time.time()
                        miaup = False
                        if tiempo > 5 :
                            tiempo -= 5
                        else :
                            tiempo = 5

                    elif event.key == K_RIGHT:
                        T1=time.time()
                        actualizar = True
                        answer.clear()
                        miaup = False
                        del lista[:]
                        if ID < 6 :
                            ID += 1            
                        else :
                            ID = 0

                    elif event.key == K_LEFT:
                        T1=time.time()
                        actualizar = True
                        answer.clear()
                        del lista[:]
                        miaup = False
                        if ID > 1 :
                            ID -= 1
                        else :
                            ID = 6

                    elif event.key == K_f:
                        T1=time.time()
                        del lista[:]
                        actualizar = True
                        ID = 0
                        miaup = False

                    elif event.key == K_j:
                        T1=time.time()
                        del lista[:]
                        actualizar = True
                        ID = 1
                        miaup = False

                    elif event.key == K_m:
                        T1=time.time()
                        del lista[:]
                        actualizar = True
                        ID = 2
                        miaup = False

                    elif event.key == K_o:
                        T1=time.time()
                        del lista[:]
                        actualizar = True
                        ID = 3
                        miaup = False

                    elif event.key == K_r:
                        T1=time.time()
                        del lista[:]
                        actualizar = True
                        ID = 4
                        miaup = False

                    elif event.key == K_s:
                        T1=time.time()
                        del lista[:]
                        actualizar = True
                        ID = 5
                        miaup = False

                    elif event.key == K_z:
                        T1=time.time()
                        del lista[:]
                        actualizar = True
                        ID = 6
                        miaup = False

                    elif event.key == K_t:
                        del lista[:]
                        answer.clear()
                        actualizar = True
                        timer = True
                        puntajes = False
                        miaup = False
                        
                    elif event.key == K_ESCAPE:
                        del lista[:]
                        answer.clear()
                        miaup = False
                        continuar = False
                        timer = False
                        puntajes = False

                elif event.type == QUIT:
                    miaup = False
                    continuar = False
                    timer = False
                    puntajes = False
        
    while timer :
        ESTADO = categorias[ID]
        Background = pygame.image.load('img/'+ESTADO.lower()+'.png').convert()
        pygame.display.set_caption("Cronometro")
        Title = font80.render("CoR 2019", True, BLANCO)
        SubT = font80.render(ESTADO.replace('_',' '), True, BLANCO)
        TT = font120.render("Tiempo: 00:00.000", True, BLANCO)
        miaut = True
        hecho = False
        pantalla.fill(BLANCO)
        pantalla.blit(Background, (0, 0))
        pantalla.blit(Title,(100,50))
        pantalla.blit(SubT,(1000,50))
        pantalla.blit(TT,(315, 350))
        pygame.display.update()


        if (llamar and online) :
            #llamado = client.open("puntajes").get_worksheet(8)
            #llamado_u = font50.render('Ahora : '+ str(llamado.cell(ID+1, 3).value), True, BLANCO)
            #llamado_d = font50.render('Sigue : '+ str(llamado.cell(ID+1, 4).value), True, BLANCO)
            try :
                llamado = client.open("puntajes").get_worksheet(HOJA_LLAMADOS)
                llamado_u = font50.render('Ahora : '+ str(llamado.cell(ID+1, 2).value), True, BLANCO)
                llamado_d = font50.render('Sigue : '+ str(llamado.cell(ID+1, 3).value), True, BLANCO)
                llamar = False
                online = True
            except :
                llamar = False
                online = False

        if online :
            pantalla.blit(llamado_u,(315, 270))
            pantalla.blit(llamado_d,(315, 470))
            pygame.display.update()

        while miaut:
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_SPACE:
                        miaut = False

                    elif event.key == K_RETURN:
                        miaut = False
                        hecho = True
                        if online :
                            llamar = True

                    elif event.key == K_p:
                        if online :
                            puntajes = True
                            timer = False 
                            hecho = True
                            miaut = False
                            llamar = True

                    elif event.key == K_f:
                        ID = 0
                        miaut = False
                        hecho = True
                        llamar = True

                    elif event.key == K_j:
                        ID = 1
                        miaut = False
                        hecho = True
                        llamar = True

                    elif event.key == K_m:
                        ID = 2
                        miaut = False
                        hecho = True
                        llamar = True

                    elif event.key == K_o:
                        ID = 3
                        miaut = False
                        hecho = True
                        llamar = True

                    elif event.key == K_r:
                        ID = 4
                        miaut = False
                        hecho = True
                        llamar = True

                    elif event.key == K_s:
                        ID = 5
                        miaut = False
                        hecho = True
                        llamar = True

                    elif event.key == K_z:
                        ID = 6
                        miaut = False
                        hecho = True
                        llamar = True

                    elif event.key == K_RIGHT:
                        llamar = True
                        if ID < 6 :
                            ID += 1
                            miaut = False
                            hecho = True
                        else :
                            ID = 0
                            miaut = False
                            hecho = True

                    elif event.key == K_LEFT:
                        llamar = True
                        if ID > 1 :
                            ID -= 1
                            miaut = False
                            hecho = True

                        else :
                            ID = 6
                            miaut = False
                            hecho = True
                
                    elif event.key == K_ESCAPE:
                        hecho = True
                        miaut = False
                        continuar = False
                        timer = False
                        puntajes = False
                        
                elif event.type == QUIT:
                    hecho = True
                    miaut = False
                    continuar = False
                    timer = False
                    puntajes = False
        T1 = time.time()
        Control1 = False
        Control2 = False
        Control3 = 0
        RText = ''
        while not hecho:
            for evento in pygame.event.get():
                if evento.type == pygame.QUIT:
                    hecho = True
                elif evento.type == KEYDOWN:
                    if evento.key == K_SPACE and not Control1:
                        try:
                            RText = OText
                            Control1 = True
                            #Control2 = False
                            Control3 = [int(Min),float(Sec)]
                        except:
                            pass
                    elif evento.key == K_SPACE and Control1:
                        Control1 = False
                        Control2 = True
                        T1 = time.time()
                    elif evento.key == K_RETURN:
                        hecho = True
                        llamar = True
                    elif evento.key == K_ESCAPE:
                        hecho = True
                        continuar = False

            if not hecho:
                pantalla.fill(BLANCO)
                pantalla.blit(Background, (0, 0))
                pantalla.blit(Title,(100,50))
                pantalla.blit(SubT,(1000,50))
                if (online) :
                    pantalla.blit(llamado_u,(315, 270))
                    pantalla.blit(llamado_d,(315, 470))

                T2 = time.time()
                if not Control2:
                    Min = str(int((T2-T1)/60))
                    Sec = str(round((T2-T1)%60,3))
                else:
                    Min = str(int(Control3[0]+(T2-T1)/60))
                    Sec = str(round(Control3[1]+(T2-T1)%60,3))
                if int(Min) < 10:
                    Min = '0' + Min
                if float(Sec)<10:
                    Sec = '0' + Sec

                OText = "Tiempo: {0}:{1}".format(Min, Sec)

                if not Control1:
                    Text = font120.render(OText, True, BLANCO)
                else:
                    Text = font120.render(RText, True, BLANCO)
                pantalla.blit(Text, [315, 350])

                pygame.display.update()

pygame.quit()