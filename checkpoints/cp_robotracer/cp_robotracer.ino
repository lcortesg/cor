#include <Keyboard.h>
#define PIN_INICIO 2
#define PIN_FINAL 3
#define PIN_RESET 4
#define LED_INICIO 12
#define LED_FINAL 13
#define NONE 0
#define key_space ' '
int state, lap_enable, key_old;

void setup() {
    state = NONE;
    lap_enable = 1;
    key_old = NONE;
    Keyboard.begin();
    pinMode(PIN_INICIO, INPUT_PULLUP);
    pinMode(PIN_FINAL, INPUT_PULLUP);
    pinMode(PIN_RESET, INPUT_PULLUP);
    pinMode(LED_INICIO, OUTPUT);
    pinMode(LED_FINAL, OUTPUT);
}

void loop() {
    if ( (key_old == NONE) && (digitalRead(PIN_RESET) == NONE) ){
            Keyboard.println("TR");
            digitalWrite(LED_INICIO, LOW);
            digitalWrite(LED_FINAL, LOW);
            lap_enable = 1;
            state = NONE;
            key_old = 1;
            delay(100);
                    
    }
    
    else if ( (lap_enable != NONE) && (digitalRead(PIN_INICIO) == NONE) && (state == NONE) ) {
        Keyboard.write(key_space);
        digitalWrite(LED_INICIO, HIGH);
        digitalWrite(LED_FINAL, LOW);
        state = 1;
        key_old = NONE;
    }

    else if ( (lap_enable != NONE) && (digitalRead(PIN_FINAL) == NONE) && (state != NONE) ){
        Keyboard.write(key_space);
        digitalWrite(LED_INICIO, HIGH);
        digitalWrite(LED_FINAL, HIGH);
        state = NONE;
        lap_enable = NONE;
        key_old = NONE; 
    }
}
